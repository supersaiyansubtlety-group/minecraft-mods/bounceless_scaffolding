package net.sssubtlety.bounceless_scaffolding;

import net.minecraft.block.Blocks;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.util.math.Vec3d;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BouncelessScaffolding {
	public static final String NAMESPACE = "bounceless_scaffolding";
	public static final Logger LOGGER = LogManager.getLogger();

	public static boolean isOnScaffolding(ClientPlayerEntity player) {
		if (player.getBlockStateAtPos().isOf(Blocks.SCAFFOLDING)) return true;
		else {
			return player.world.getBlockState(player.getBlockPos().down()).isOf(Blocks.SCAFFOLDING);
//			if (!player.world.getBlockState(player.getBlockPos().down()).isOf(Blocks.SCAFFOLDING)) return false;
//			final double y = player.getPos().y;
//			final double yDecimal = y - (int)y;
//			return yDecimal < 0.01;
		}
	}
}
