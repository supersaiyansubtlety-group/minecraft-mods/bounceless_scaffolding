package net.sssubtlety.bounceless_scaffolding.mixin_helpers;

public interface FakeSneaker {
    void setFakeSneaking(boolean fakeSneaking);
    boolean isFakeSneaking();
}
