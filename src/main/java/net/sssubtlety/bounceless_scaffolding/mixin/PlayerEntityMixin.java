package net.sssubtlety.bounceless_scaffolding.mixin;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import net.sssubtlety.bounceless_scaffolding.mixin_helpers.FakeSneaker;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity implements FakeSneaker {
    private boolean fakeSneaking;

    protected PlayerEntityMixin(EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
        throw new IllegalStateException("PlayerEntityMixin's dummy constructor called!");
    }

    @Override
    public void setFakeSneaking(boolean fakeSneaking) {
        this.fakeSneaking = fakeSneaking;
    }

    @Override
    public boolean isFakeSneaking() {
        return this.fakeSneaking;
    }

    @Inject(method = "shouldCancelInteraction", at = @At("RETURN"), cancellable = true)
    private void bounceless_scaffolding$postShouldCancelInteraction(CallbackInfoReturnable<Boolean> cir) {
        if (!this.world.isClient) return;
        if (fakeSneaking) cir.setReturnValue(true);
    }
}
