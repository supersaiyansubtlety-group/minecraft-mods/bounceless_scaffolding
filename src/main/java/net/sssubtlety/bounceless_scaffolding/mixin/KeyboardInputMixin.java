package net.sssubtlety.bounceless_scaffolding.mixin;

import net.minecraft.block.Blocks;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.input.Input;
import net.minecraft.client.input.KeyboardInput;
import net.minecraft.client.network.ClientPlayerEntity;
import net.sssubtlety.bounceless_scaffolding.BouncelessScaffolding;
import net.sssubtlety.bounceless_scaffolding.mixin_helpers.FakeSneaker;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static net.sssubtlety.bounceless_scaffolding.BouncelessScaffolding.LOGGER;

@Mixin(KeyboardInput.class)
public class KeyboardInputMixin extends Input {
    @Inject(method = "tick", at = @At("TAIL"))
    private void bounceless_scaffolding$postTick(CallbackInfo ci) {
        final ClientPlayerEntity player = MinecraftClient.getInstance().player;
        if (player == null) return;
//        jumping = false;
//        sneaking = false;
        if (jumping && sneaking && BouncelessScaffolding.isOnScaffolding(player)) {
            ((FakeSneaker) player).setFakeSneaking(true);
            jumping = false;
            sneaking = false;
//            LOGGER.error("jump+sneak on scaffolding!");
        } else {
            ((FakeSneaker) player).setFakeSneaking(false);
//            LOGGER.info("NOT jump+sneak on scaffolding!");
        }
    }
}
