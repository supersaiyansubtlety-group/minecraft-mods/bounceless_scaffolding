package net.sssubtlety.bounceless_scaffolding.mixin;

import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.network.packet.c2s.play.PlayerInputC2SPacket;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.sssubtlety.bounceless_scaffolding.mixin_helpers.FakeSneaker;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ClientPlayerInteractionManager.class)
public class ClientPlayerInteractionManagerMixin {
    @Inject(method = "interactBlock", at = @At(value = "INVOKE", target = "Lorg/apache/commons/lang3/mutable/MutableObject;<init>()V"))
    private void bounceless_scaffolding$preSendInteractionPacket(ClientPlayerEntity player, Hand hand, BlockHitResult hitResult, CallbackInfoReturnable<ActionResult> cir) {
        if (((FakeSneaker)player).isFakeSneaking()) {
            player.networkHandler.sendPacket(new PlayerInputC2SPacket(player.sidewaysSpeed, player.forwardSpeed, player.input.jumping, true));
        }
    }

    @Inject(method = "interactBlock", at = @At("TAIL"))
    private void bounceless_scaffolding$postSendInteractionPacket(ClientPlayerEntity player, Hand hand, BlockHitResult hitResult, CallbackInfoReturnable<ActionResult> cir) {
        if (((FakeSneaker)player).isFakeSneaking()) {
            player.networkHandler.sendPacket(new PlayerInputC2SPacket(player.sidewaysSpeed, player.forwardSpeed, player.input.jumping, false));
        }
    }
}
